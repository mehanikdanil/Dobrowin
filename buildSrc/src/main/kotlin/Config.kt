import org.gradle.api.JavaVersion

object Config {

    const val compileSdk = 34
    const val minSDK = 24
    const val targetSDK = 34

    const val release = "release"
    const val debug = "debug"
    const val packageName = "com.dobrowin.game.pro"

    object Options {
        val compileOptions = JavaVersion.VERSION_11
        const val kotlinOptions = "11"
    }

}